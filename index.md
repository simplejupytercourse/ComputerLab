# Simple Jupyter Course
Provide folders and files required to demo a simple course using JupyterCourse and the Travo Dashboards. 

In particular: 
 1. In the Instructor Dashboard, "generate" and "release" of an example simple assignment; 
 2. In the Student Dashboard, "fetch", modify and "submit" the assignment; 
 3. Back in the Instructor Dashboard, "collect" the submissions.
