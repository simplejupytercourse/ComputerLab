#!/usr/bin/env python
from travo import GitLab
from travo.jupyter_course import JupyterCourse
from travo.script import main

course = JupyterCourse(

    # The URL of the forge that will host the course
    # forge=GitLab("https://gitlab.com"),

    # The root group on the forge that will host the course
    path="SimpleJupyterCourse",

    # A human redable name for the course
    # Used as name for the root group, as well as in the Travo user interface
    # Warning: there are some limitations on groups names:
    # https://docs.gitlab.com/ee/user/reserved_names.html#limitations-on-usernames-project-and-group-names
    name="Simple Jupyter Course",

    # Optional: the web page of the course (uncomment the line below to activate)
    # url="http://example.com/simplejupytercourse",

    # Optional: the list of the student groups (uncomment the line below to activate)
    # Recommendation: use only alphanumeric characters
    # student_groups=["group1", "group2"],

    # The name of the current session for the course.
    # Assignments will be published in <path>/<session_path>
    # Warning: do not change the value once student have started
    # submitting assignments, or they will have several submissions
    # for the same session
    session_path="2024-2025",

    # The date at which the access of instructors to the student's
    # submissions will automatically expire
    expires_at="2025-12-31",

    # Recommended default values
    group_submissions=True,
    student_dir="./",
    script="./course.py",
    assignments = ["Lesson1", "Lesson2"]                                                                                           
)

if __name__ == "__main__":
    main(course, "")
